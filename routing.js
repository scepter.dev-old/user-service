const express = require('express');
const router = express.Router();
const admin = require("firebase-admin");
require('dotenv').config();

admin.initializeApp({
    credential: admin.credential.cert(JSON.parse(process.env.FIREBASE_CREDENTIALS)),
    databaseURL: "https://scepter-26dc3.firebaseio.com"
});

router.get('/users/me', (req, res) => {
    if(!req.headers.user){
        return res.status(401).json();
    }

    admin.firestore().collection('users').doc(req.headers.user).get().then(snapshot => {
        const user = snapshot.data();
        user.id = req.headers.user;
        return res.status(200).json(user);
    }).catch(error => {
        console.log(error);
        return res.status(500).json(error)
    });
});

router.get('/users',async (req, res) => {
    if(!req.headers.user){
        return res.status(401).json();
    }

    if(!req.query.ids || !Array.isArray(req.query.ids)) {
        return res.status(400).json();
    }

    const users = [];

    for(let i = 0; i < req.query.ids.length; i++) {
        const doc = await admin.firestore().collection('users').doc(req.query.ids[i]).get().then(snapshot => {
            return snapshot.data();
        });

        if(doc != null){
            doc.id = req.query.ids[i];
            users.push(doc);
        }
    }
    return res.status(200).json(users);
});

router.get('/users/search/:email', async(req, res) => {
    if(!req.headers.user) {
        return res.status(401).json();
    }

    const query = await admin.firestore().collection('users').where('email', '=', req.params.email).get();
    if(!query.empty) {
        return res.status(200).json(query.docs[0].data());
    } else {
        return res.status(404).json({message: 'user not found'});
    }
});

module.exports = router;
