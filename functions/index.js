const functions = require('firebase-functions');
const admin = require("firebase-admin");
const md5 = require("md5");

admin.initializeApp(functions.config().firebase);

const database = admin.firestore();

exports.createUser = functions.auth.user().onCreate(user=> {
    return database.collection('users').doc(user.uid).set({email: user.email, photoURL: user.photoURL || `https://gravatar.com/avatar/${md5(user.email)}`});
});

exports.deleteUser = functions.auth.user().onDelete(user => {
   return database.collection('users').doc(user.uid).delete();
});
