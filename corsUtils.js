require('dotenv').config();

module.exports = {
    getOrigins: () => {
        const origins = [];
        const envOrigins = (process.env.CORS_ORIGINS || 'http://localhost:8080').split(',');
        envOrigins.forEach(origin => {
            origins.push(origin);
        });
        return origins;
    }
};
