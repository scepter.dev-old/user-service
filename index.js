const app = require('express')();
const port = process.env.PORT || 3002;
const routing = require("./routing");

app.use('/', routing);

app.listen(port, () => console.log(`User service started at port ${[port]}`));
